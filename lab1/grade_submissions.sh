#!/bin/bash
# lab1 grading automation script

# rubric:
# 1. main compiles                     9 points
# 2.1 main compiles & txt exists       1 point
# 2.2 main compiles & txt correct      2 point
# 2.3 main compiles & stdout correct   2 points
# 2.4 main compiles & stdout=txt       1 points
# 3. main contains header comment      1 point
# 4. main has comments, good naming    1 point
# 5. main has good formatting          1 point
# 6. readme has all information        2 points
# total - 20 points

# how does this program aid in grading?
# 1, 2: automatic score calculation
# 3, 4, 5: prints out main, prompts for feedback
# 6: prints out readme, prompts for feedback
# this script also generates a CSV of all the final grades

# fail on undefined variable reference
#set -u

ASSIGNMENT="Lab 1"
MAIN=Leap
# python 2 can run python 3 code, but not vice verse
PYTHON=$(which python2)
DIFF="/usr/bin/diff -b"

README=README.txt
TXT=Output.txt

EXPECTED_OUTPUT=expected_output.txt
ACTUAL_OUTPUT=actual_output.txt

ACTUAL_DIFF=actual_vs_expected.diff
TXT_DIFF=txt_vs_actual.diff

REPORT=rubric_report.txt
GRADE=final_grade.txt
STUDENT_NAME=student_name.txt

EMPTY_SUBMISSIONS=empty_submissions.txt
CSV=final_grades.csv
CSV_COLUMNS="Student,Student ID,Section,${ASSIGNMENT}"

COMPILE_MAX_POINTS=9
COMPILE_CRITERIA="${MAIN} compiles without errors"

TXT_EXISTS_MAX_POINTS=1
TXT_EXISTS_CRITERIA="${MAIN} compiles and ${TXT} exists"

TXT_CORRECT_MAX_POINTS=2
TXT_CORRECT_CRITERIA="${MAIN} compiles and ${TXT} is correct"

STDOUT_CORRECT_MAX_POINTS=2
STDOUT_CORRECT_CRITERIA="${MAIN} compiles and stdout is correct"

STDOUT_IS_TXT_MAX_POINTS=1
STDOUT_IS_TXT_CRITERIA="${MAIN} compiles and stdout matches ${TXT}"

HEADER_MAX_POINTS=1
HEADER_CRITERIA="header contains name, cruzid, date, assignment, class, description"

STYLE_MAX_POINTS=1
STYLE_CRITERIA="${MAIN} has useful & sufficient comments, descriptive variable names"

STRUCTURE_MAX_POINTS=1
STRUCTURE_CRITERIA="${MAIN} has clean visual structure / use of white space"

README_MAX_POINTS=2
README_CRITERIA="${README} contains assignment, class, name, cruzid, language"

die(){
  echo $1
  exit 1
}

usage(){
  cat <<EOF

usage: $(basename $0) submission_dirs...

  A submission dir is a directory named after a cruzid
  Example invocation:

    $ $(basename $0) zgbradle miradova wrcooper

  This scripts expects a file called "${EXPECTED_OUTPUT}" to be located
  in the working directory. This is the source of truth to compare
  submission outputs against.

  This script will write the following files to each submission dir:
    - ${ACTUAL_OUTPUT}
      Output from running ${MAIN}

    - ${ACTUAL_DIFF}
      The difference between "${ACTUAL_OUTPUT}" and "${EXPECTED_OUTPUT}"

    - ${TXT_DIFF}
      The difference between "${TXT}" and "${ACTUAL_OUTPUT}"

    - ${REPORT}
      Points earned by the submission for each rubric criteria

    - ${GRADE}
      The sum of all of a submission's points

    - ${STUDENT_NAME}
      The name of the student who submitted the submission.
      This is automatically parsed from the ${README}, but can be
      interactively corrected if ${README} isn't formatted correctly.

      If the submission has no ${README}, this falls back to the submission name.

  This script will also generate the following files in the working directory:
    - ${CSV}
      CSV of all final grades.

    - ${EMPTY_SUBMISSIONS}
      The names of all empty submissions.
EOF
  die
}

check_binary(){
  command -v $1 >/dev/null 2>&1 || die "$1 doesn't exist in path"
}

safety_check(){
  check_binary diff
  check_binary $PYTHON
  check_binary gcc
  check_binary java
  check_binary javac
  check_binary dos2unix
  test -f $EXPECTED_OUTPUT || die "$EXPECTED_OUTPUT doesn't exist"
}

log(){
  echo ">>> $1"
}

prompt(){
  printf ">>> $1: "
}

append_report(){
  # usage: append_report SUBMISSION_DIR POINT "blah blah criteria blah blah"
  local SUBMISSION=$1
  local POINT=$2
  shift 2
  local CRITERIA="$@"
  echo "${POINT} - ${CRITERIA}" >> ${SUBMISSION}/${REPORT}
}

report_to_grade(){
  # a report is a table of points recieved per criteria
  # a grade is the sum of these points
  local SUBMISSION=$1
  local SUM=0
  local POINT
  local CRITERIA

  # we don't really care about the criteria
  while read POINT CRITERIA; do
    SUM=$((SUM+POINT))
  done < ${SUBMISSION}/${REPORT}

  echo $SUM > ${SUBMISSION}/${GRADE}
}

append_csv(){
  local SUBMISSION=$1
  local NAME
  local SCORE
  read NAME < ${SUBMISSION}/${STUDENT_NAME}
  read SCORE < ${SUBMISSION}/${GRADE}

  # turns out you need the Student ID and Section columns, but no actual data
  echo "\"${NAME}\",null,null,${SCORE}" >> ${CSV}
}

bad_main(){
  # main doesn't compile, so these are automatic 0s
  local CRITERIA
  for CRITERIA in "${COMPILE_CRITERIA}" "${TXT_EXISTS_CRITERIA}" \
                  "${TXT_CORRECT_CRITERIA}" "${STDOUT_CORRECT_CRITERIA}" \
                  "${STDOUT_IS_TXT_CRITERIA}"; do
    append_report ${SUBMISSION} 0 "${CRITERIA}"
  done
}

no_main(){
  # main doesn't exist, let alone compile, so these are automatic 0s
  local SUBMISSION=$1
  local CRITERIA
  bad_main
  for CRITERIA in  "${HEADER_CRITERIA}" "${STYLE_CRITERIA}" \
                   "${STRUCTURE_CRITERIA}"; do
    append_report ${SUBMISSION} 0 "${CRITERIA}"
  done
}

grade_outputs(){
  local SUBMISSION=$1
  local TXT_EXISTS_POINTS=0
  local TXT_CORRECT_POINTS=0
  local STDOUT_CORRECT_POINTS=0
  local STDOUT_IS_TXT_POINTS=0

  # does txt output exist?
  if test -f ${SUBMISSION}/${TXT}; then
    log "${SUBMISSION}: ${TXT} exists"
    dos2unix ${SUBMISSION}/${TXT} >/dev/null 2>&1
    # give points for existance
    TXT_EXISTS_POINTS=${TXT_EXISTS_MAX_POINTS}

    # give points for correctness
    $DIFF ${SUBMISSION}/${TXT} ${EXPECTED_OUTPUT} > ${SUBMISSION}/${TXT_DIFF}
    local DIFF_EXIT_CODE=$?
    test ${DIFF_EXIT_CODE} -eq 0 && TXT_CORRECT_POINTS=${TXT_CORRECT_MAX_POINTS}

    # give points if txt matches actual output
    $DIFF ${SUBMISSION}/${ACTUAL_OUTPUT} ${SUBMISSION}/${TXT} > ${SUBMISSION}/${TXT_DIFF}
    local TXT_DIFF_EXIT_CODE=$?
    test ${TXT_DIFF_EXIT_CODE} -eq 0 && STDOUT_IS_TXT_POINTS=${STDOUT_IS_TXT_MAX_POINTS}
  else
    log "${SUBMISSION}: ${TXT} does not exist"
  fi

  # test if main has correct stdout
  $DIFF ${SUBMISSION}/${ACTUAL_OUTPUT} ${EXPECTED_OUTPUT} > ${SUBMISSION}/${ACTUAL_DIFF}
  local ACTUAL_DIFF_EXIT_CODE=$?
  test ${ACTUAL_DIFF_EXIT_CODE} -eq 0 && STDOUT_CORRECT_POINTS=${STDOUT_CORRECT_MAX_POINTS}

  append_report ${SUBMISSION} ${TXT_EXISTS_POINTS} "${TXT_EXISTS_CRITERIA}"
  append_report ${SUBMISSION} ${TXT_CORRECT_POINTS} "${TXT_CORRECT_CRITERIA}"
  append_report ${SUBMISSION} ${STDOUT_IS_TXT_POINTS} "${STDOUT_IS_TXT_CRITERIA}"
  append_report ${SUBMISSION} ${STDOUT_CORRECT_POINTS} "${STDOUT_CORRECT_CRITERIA}"
}

compile_lang(){
  local SUBMISSION=$1
  local LANG=$2
  local EXIT_CODE

  case $LANG in
    python)
      $PYTHON ${SUBMISSION}/${MAIN}.py >/dev/null 2>&1
      EXIT_CODE=$? ;;
    c)
      gcc ${SUBMISSION}/${MAIN}.c -o ${SUBMISSION}/a.out >/dev/null 2>&1
      EXIT_CODE=$? ;;
    java)
      # change directories, because java is weird...
      pushd ${SUBMISSION} >/dev/null
      export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8
      javac ${MAIN}.java >/dev/null 2>&1
      EXIT_CODE=$?
      popd >/dev/null ;;
  esac
  return ${EXIT_CODE}
}

run_lang(){
  local SUBMISSION=$1
  local LANG=$2
  case $LANG in
    python)
      $PYTHON ${SUBMISSION}/${MAIN}.py > ${SUBMISSION}/${ACTUAL_OUTPUT} 2>&1 ;;
    c)
      ./${SUBMISSION}/a.out > ${SUBMISSION}/${ACTUAL_OUTPUT} 2>&1 ;;
    java)
      pushd ${SUBMISSION} >/dev/null
      java ${MAIN} > ${ACTUAL_OUTPUT} 2>/dev/null
      popd >/dev/null
      ;;
  esac
}

grade_lang(){
  local SUBMISSION=$1
  local LANG=$2

  if compile_lang ${SUBMISSION} ${LANG}; then
    log "${SUBMISSION}: ${MAIN} compiles"
    append_report ${SUBMISSION} ${COMPILE_MAX_POINTS} "${COMPILE_CRITERIA}"
    run_lang ${SUBMISSION} ${LANG}
    grade_outputs ${SUBMISSION}
  else
    log "${SUBMISSION}: ${MAIN} failed to compile"
    bad_main ${SUBMISSION}
  fi

  local EXT
  case ${LANG} in
    python) EXT=py;;
    java) EXT=java;;
    c) EXT=c;;
  esac
  inspect_main ${SUBMISSION}/${MAIN}.${EXT}
}

is_int(){
  echo "$1" | grep -qE '^[0-9]+$'
}

copy_points(){
  # prompt for point input, and then write to report
  local SUBMISSION="$1"
  local SECTION="$2"
  local CRITERIA="$3"
  local MAX_POINTS="$4"
  local POINTS

  while :; do
    log "${SECTION} criteria: ${CRITERIA}"
    prompt "points (max=${MAX_POINTS})"
    read POINTS
    is_int ${POINTS} && test ${POINTS} -le ${MAX_POINTS} && break
  done

  append_report ${SUBMISSION} ${POINTS} "${CRITERIA}"
}

inspect_main(){
  local MAIN=$1
  local SUBMISSION=$(dirname ${MAIN})

  log "${SUBMISSION}: now inspecting main"
  log "begin main"
  echo
  cat ${MAIN}
  echo
  log "end main"

  copy_points ${SUBMISSION} HEADER "${HEADER_CRITERIA}" ${HEADER_MAX_POINTS}
  copy_points ${SUBMISSION} STYLE "${STYLE_CRITERIA}" ${STYLE_MAX_POINTS}
  copy_points ${SUBMISSION} STRUCTURE "${STRUCTURE_CRITERIA}" ${STRUCTURE_MAX_POINTS}
}

grade_main(){
  local SUBMISSION=$1
  if test -f ${SUBMISSION}/${MAIN}.py; then
    log "${SUBMISSION}: ${MAIN} is python source"
    grade_lang ${SUBMISSION} python

  elif test -f ${SUBMISSION}/${MAIN}.c; then
    log "${SUBMISSION}: ${MAIN} is c source"
    grade_lang ${SUBMISSION} c

  elif test -f ${SUBMISSION}/${MAIN}.java; then
    log "${SUBMISSION}: ${MAIN} is java source"
    grade_lang ${SUBMISSION} java

  else
    log "${SUBMISSION}: no $MAIN found"
    no_main ${SUBMISSION}
  fi
}

inspect_readme(){
  local SUBMISSION=$1
  local NAME
  local NEW_NAME
  if test -f ${SUBMISSION}/${README}; then
    log "${SUBMISSION}: $README found"
    log "begin ${README}"
    echo
    cat ${SUBMISSION}/${README}
    echo
    log "end ${README}"

    NAME="$(sed -n 5p ${SUBMISSION}/${README})"
    log "student name detected as \"${NAME}\""
    log "THIS MUST BE FIXED IF INCORRECT"
    log "please type the correct name, or just hit enter if its already correct"
    prompt "name (last, first)"
    read NEW_NAME
    # only change NAME if NEW_NAME is non empty string
    test -n "${NEW_NAME}" && NAME="${NEW_NAME}"
    echo ${NAME} > ${SUBMISSION}/${STUDENT_NAME}

    copy_points ${SUBMISSION} README "${README_CRITERIA}" ${README_MAX_POINTS}
  else
    log "${SUBMISSION}: $README does not exist"
    append_report ${SUBMISSION} 0 "${README_CRITERIA}"
    # fall back to cruzid as name
    basename ${SUBMISSION} > ${SUBMISSION}/${STUDENT_NAME}
  fi
}

# only run main when this script is run by `bash`, not `source`
echo "$-" | grep -q i && die

safety_check
test -z "$1" && usage

log "WARNING: overwriting ${EMPTY_SUBMISSIONS}, ${CSV}, and all submission files"
prompt "press enter to continue"
read
echo ${CSV_COLUMNS} > ${CSV}

for SUBMISSION in $@; do
  # delete any existing reports and csv's
  rm -f ${SUBMISSION}/${REPORT}
  rm -f ${EMPTY_SUBMISSIONS}

  if [[ -z $(ls -A ${SUBMISSION}) ]]; then
    log "${SUBMISSION}: empty"
    basename ${SUBMISSION} >> ${EMPTY_SUBMISSIONS}
    continue
  fi

  log "${SUBMISSION}: begin grading"
  grade_main ${SUBMISSION}
  inspect_readme ${SUBMISSION}
  report_to_grade ${SUBMISSION}
  append_csv ${SUBMISSION}
  clear
done
